import numpy as np
import pulp
from std_lp import StandardFormLP
from two_phase_simplex import TwoPhaseSimplex

class LP(object):
    def __init__(self):
        self.A = None
        self.b = None
        self.c = None
        self.x = None
        self.minimization = None

    def get_constraint_components(self, pulp_lp, cons):
        return pulp_lp.constraints[cons].getLb(), \
               pulp_lp.constraints[cons].getUb(), \
               list(pulp_lp.constraints[cons].items())

    def process_constraints(self, pulp_lp):
        constraints = pulp_lp.constraints
        constraint_num = 0
        for constraint in constraints:
            is_inequality_constraint = None
            lb, ub, terms = self.get_constraint_components(pulp_lp, constraint)

            if lb is None or ub is None:
                is_inequality_constraint = True
            else:
                is_inequality_constraint = False

            for term in terms:
                term_variable = str(term[0])
                term_index = int(term_variable.split('_')[1])
                term_coefficient = term[1]
                self.A[constraint_num][term_index] = term_coefficient

            if is_inequality_constraint:
                try:
                    if (lb is not None):
                        ineq_direction = '>'
                        self.b[constraint_num] = lb
                        self.bdir[constraint_num] = ineq_direction
                    elif (ub is not None):
                        ineq_direction = '<'
                        self.b[constraint_num] = ub
                        self.bdir[constraint_num] = ineq_direction
                except AppError as error:
                    logger.error(error)
                    raise
            else:
                ineq_direction = '='
                self.b[constraint_num] = lb
                self.bdir[constraint_num] = ineq_direction

            constraint_num += 1

    def process_objective(self, pulp_lp):
        if pulp_lp.sense == pulp.LpMinimize:
            self.minimization = 'min'
        else:
            self.minimization = 'max'

        terms = list(pulp.LpConstraint(pulp_lp.objective).items())

        for term in terms:
            term_variable = str(term[0])
            term_index = int(term_variable.split('_')[1])
            term_coefficient = term[1]
            self.c[term_index] = term_coefficient

    def process_variables(self, pulp_lp):
        for variable in pulp_lp.variables():
            variable_idx, lb, ub = int(str(variable).split('_')[1]), variable.getLb(), variable.getUb()

            self.x[variable_idx] = '>' # keep all variables unbounded for now
            # encode variable bounds as constraints

    def convert_PULP_LP(self, pulp_lp):
        '''
        Convert a PuLP Linear Program Constraints to Simplex Constraints
        '''
        num_variables = pulp_lp.numVariables()
        num_constraints = pulp_lp.numConstraints()
        print("n", num_variables, "m", num_constraints)
        self.A = np.zeros([num_constraints, num_variables], dtype=np.float32)
        self.b = np.zeros(num_constraints, dtype=np.float64)
        self.bdir = [''] * num_constraints
        self.c = np.zeros(num_variables, dtype=np.float64)
        # 1 constraint LHS, matrix A
        # 2 constraint RHS, matrix b
        self.process_constraints(pulp_lp)

        # 3 objective coefficients, c
        self.process_objective(pulp_lp)
        # 4 variable bounds
        self.x = [''] * num_variables
        self.process_variables(pulp_lp)

        print(self.A[-1:])
        print(self.b)
        print(self.bdir)
        print(self.minimization, self.c)
        print(self.x)

    def check_valid_lp(self):
        '''
            Checks if an LP is valid. By valid we mean that the number constraints
            and variables match in all four files.
        '''
        num_constraints, num_variables = self.A.shape
        if len(self.x) != num_variables:
            return False, 'Number of variables in A and x do not match.'
        if len(self.bdir) != num_constraints or len(self.b) != num_constraints:
            return False, 'Number of constraints in A and b do not match.'
        if len(self.c) != num_variables:
            return False, 'Number of variables in A and c do not match.'
        return True, ''

    def is_lp_valid(self):
        # Check if the LP is valid.
        verbose = True
        valid, msg = self.check_valid_lp()
        if not valid:
            err_msg = msg if verbose else 'Invalid LP. Exiting.'
            print(err_msg)
            exit(0)

    def convert_to_signed_string(self, num):
        ''' Converts a floating point number to a signed string. '''
        string = '+' if num >= 0 else ''
        return string + str(num)

    def simplex(self, verbose):
        model = StandardFormLP(self.x, self.A, self.bdir, self.b, self.minimization, self.c)
        if model.are_dependent_constraints():
            print('Dependent constraints. Exiting.')
            exit()
        solver = TwoPhaseSimplex(verbose=verbose)
        solution, obj, x, table, objvec, bfs = solver.solve(model)

        orig_objective = 0.0
        orig_vars = 0.0
        if solution == 'Infeasible':
            print('Infeasible problem. Exiting.')
        elif solution == 'Unbounded':
            print('Unbounded problem. Exiting.')
        elif solution == 'Solved':
            orig_objective = (self.convert_to_signed_string(model.convert_objective(obj)))
            orig_vars = list(map(self.convert_to_signed_string, model.convert_variables(x)))

            print(self.convert_to_signed_string(model.convert_objective(obj)))
            print('\t'.join(list(map(self.convert_to_signed_string, model.convert_variables(x)))))
        else:
            # Solutions should be either Infeasible, Unbounded or Solved.
            assert False
        return solution, float(orig_objective), orig_vars, table, objvec, model, bfs

    def simplex_phase2(self, verbose, simplex_table, model, bfs):
        solver = TwoPhaseSimplex(verbose=verbose)
        solution, obj, x, table, objvec = solver._phase2_wrapper(simplex_table, model, bfs)

        orig_objective = 0.0
        orig_vars = 0.0
        if solution == 'Infeasible':
            print('Infeasible problem. Exiting.')
        elif solution == 'Unbounded':
            print('Unbounded problem. Exiting.')
        elif solution == 'Solved':
            orig_objective = (self.convert_to_signed_string(model.convert_objective(obj)))
            orig_vars = list(map(self.convert_to_signed_string, model.convert_variables(x)))

            print(self.convert_to_signed_string(model.convert_objective(obj)))
            print('\t'.join(list(map(self.convert_to_signed_string, model.convert_variables(x)))))
        else:
            # Solutions should be either Infeasible, Unbounded or Solved.
            assert False
        return solution, float(orig_objective), orig_vars, table, objvec, model, bfs

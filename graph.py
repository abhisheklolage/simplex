import numpy as np
import random
import queue
import math

import pulp
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from networkx.algorithms.connectivity import minimum_st_edge_cut
import itertools

class Graph(object):

    def __init__(self, V):
        self.num_vertices = V
        self.num_edges = 0
        self.adjacency_matrix = np.zeros((V, V), dtype=int)

    def init_adjacency(self, A):
        self.edges = []
        edges_added = 0
        self.adjacency_matrix = np.asarray(A)

        for ii in range(self.num_vertices):
            for jj in range(self.num_vertices):
                if (self.adjacency_matrix[ii][jj] is not 0):
                    self.edges.append([ii, jj])
                    edges_added += 1
        self.num_edges = edges_added

    def init_complete(self):
        self.edges = []
        edges_added = 0
        weight = 0.0
        for ii in range(self.num_vertices):
            for jj in range(ii):
                    weight = random.randrange(10)
                    self.adjacency_matrix[ii, jj] = weight
                    self.adjacency_matrix[jj, ii] = weight
                    self.edges.append([ii, jj])
                    self.edges.append([ii, jj])
                    edges_added += 1
        self.num_edges = edges_added

    def bfs(self, start):
        visited = [False] * self.num_vertices
        q = queue.Queue()
        q.put(start)
        visited[start] = True
        print(self.num_vertices)
        while not q.empty():
            current = q.get()
            for ii in range(self.num_vertices):
                if self.adjacency_matrix[current, ii] == 1:
                    if not visited[ii]:
                        q.put(ii)
                        visited[ii] = True
        return visited

    def is_connected(self):
        visited = self.bfs(0)
        for v in visited:
            if v is not True:
                return False
        return True

    def __str__(self):
        return str(self.adjacency_matrix)

'''
Abhishek Lolage
CSCI 5114 Practical Algorithmic Copmlexity
'''

'''
Python modules
'''
import numpy as np
import sys
np.set_printoptions(threshold=sys.maxsize)
import random
import queue
import math
import itertools
import numpy as np
import matplotlib.pyplot as plt
import math
import copy

import pulp
import networkx as nx
from networkx.algorithms.connectivity import minimum_st_edge_cut

'''
Implemented Modules
'''
import graph
import graph_examples
from pulp_to_simplex import LP

class TspSolver(object):
    def __init__(self, G):
        self.G = G
        self.cut_constraints = []
        self.vcount = 0
        self.vname_edge = {}
        self.edge_vname = {}
        self.simplex_solution = None
        self.simplex_obj = None
        self.simplex_vars = None
        self.simplex_table = None
        self.simplex_objvec = None
        self.simplex_model = None
        self.simplex_bfs = None

    def create_initial_tsp_LP(self, relax):
        print("Created empty minimizer LP")
        self.LP = pulp.LpProblem("TSP LP", pulp.LpMinimize)
        self.objective = ''
        print("Creating variables")
        relaxation = relax

        self.create_variables(relaxation)
        self.add_variable_upper_bound_constraints()

        print("Adding objective x_i_j * c_i_j")
        self.add_objective()

        print("Adding degree 2 constraints")
        self.add_deg2_constraints()

        self.subtour_cons = []
        self.cutting = []

    def make2dlist(self, rows, cols):
        return [([None] * cols) for row in range(rows)]

    def get_variable_name(self, v1, v2):
        return 'x_' + str(v1 * self.G.num_vertices + v2)

    def get_next_variable_name(self):
        vname = 'x_' + str(self.vcount)
        self.vcount += 1
        return vname

    def create_variable_for_edge(self, v1, v2, relaxation):
        variable_name = self.get_next_variable_name()
        self.vname_edge[variable_name] = [v1, v2]
        self.edge_vname[(v1, v2)] = variable_name
        if relaxation:
            return pulp.LpVariable(variable_name, lowBound=0.0, cat='Continuous')
        else:
            return pulp.LpVariable(variable_name, cat='Binary')

    def create_variables(self, relaxation):
        V = self.G.num_vertices
        self.variables = self.make2dlist(V, V)
        for v1 in range(V):
            for v2 in range(V):
                if v1 is not v2:
                    if (v2 > v1):
                        self.variables[v1][v2] = self.create_variable_for_edge(v2, v1, relaxation)

    def get_variable(self, v1, v2):
        if (v1 < v2):
            return self.variables[v1][v2]
        else:
            return self.variables[v2][v1]

    def add_objective(self):
        V = self.G.num_vertices
        objective = None
        for v1 in range(V):
            for v2 in  range(V):
                if self.get_variable(v1, v2) is not None:
                    objective += (self.get_variable(v1, v2) * self.G.adjacency_matrix[v1][v2])
        self.LP += objective, "OBJ"

    def add_variable_upper_bound_constraints(self):
        for v1 in range(V):
            for v2 in range(V):
                if v1 is not v2:
                    if (v2 > v1):
                        self.LP += self.get_variable(v1, v2) <= 1

    def add_deg2_constraints(self):
        V = self.G.num_vertices
        self.deg2cons = []
        for v1 in range(V):
            deg2constraint = None
            for v2 in range(V):
                if self.get_variable(v1, v2) is not None:
                    deg2constraint += self.get_variable(v1, v2)
            if deg2constraint is not None:
                self.LP += (deg2constraint == 2)
                self.deg2cons.append(deg2constraint == 2)

    def sum_over_pairs_lhs(self, set1, set2):
        sum_over_pairs_lhs = None
        for x, y in list(itertools.product(set1, set2)):
            sum_over_pairs_lhs += self.get_variable(x, y)
        return sum_over_pairs_lhs

    def add_subtour_elimination_constraint(self, partition):
        self.cut_constraints.append(partition)
        set1 = partition[0]
        set2 = partition[1]
        self.subtour_cons.append(self.sum_over_pairs_lhs(set1, set2) >= 2)
        self.LP += self.sum_over_pairs_lhs(set1, set2) >= 2

    def connected_components(self, G):
        ccs = [G.subgraph(cc) for cc in sorted(nx.connected_components(G), key=len, reverse=True)]
        return ccs

    def mincut(self, cut_graph):
        #1
        # if connected components = 1, then find cut with value < 2.0

        print("Finding a min cut with value < 2.0 to add as a constraint")
        V = self.G.num_vertices
        partition = []
        cut_value = -1
        for v1 in range(V):
            for v2 in range(v1):
                cut_value, partition = nx.minimum_cut(cut_graph, v1, v2)
                if ((cut_value - 2.0) < (-0.001)):
                    print("Cut Value:", cut_value)
                    print("Cut:", partition)
                    self.add_subtour_elimination_constraint(partition)
                    return False
        print("No such cut")
        return True

    def subtour_constraint(self, ccs):
        #2
        # connected components
        for ii in range(len(ccs)):
            cc1_nodes = ccs[ii].nodes()
            other_nodes = []
            for jj in range(len(ccs)):
                if jj != ii:
                    other_nodes += ccs[jj].nodes()
            print(cc1_nodes, other_nodes)
            self.LP += self.sum_over_pairs_lhs(cc1_nodes, other_nodes) >= 2
        return False

    def separation_oracle(self):
        V = self.G.num_vertices
        cut_graph = nx.Graph()
        #cut_graph.add_edges_from(self.varvalues)

        for ii in range(V):
            for jj in range(V):
                if (self.varvalues[ii][jj] is not None and self.varvalues[ii][jj] > 0.0):
                    cut_graph.add_edge(ii, jj, capacity=self.varvalues[ii][jj])

        ccs = self.connected_components(cut_graph)
        print("# of connected components =", len(ccs))
        #if (len(ccs) <= 2):
        if True:
            #1
            return self.mincut(cut_graph)
        elif len(ccs) > 2:
            #2
            #return self.subtour_constraint(ccs)
            pass

    def solve_lp(self, solver, gomory_stage=False):

        V = self.G.num_vertices
        self.varvalues = self.make2dlist(V, V)
        self.varvaluesintegral = []
        self.varvaluesfractional = []
        self.edge_colors = []

        if (solver == 'simplex'):
            # Simplex
            lp = LP()

            if (gomory_stage is False):
                lp.convert_PULP_LP(self.LP)
                lp.is_lp_valid()
                solution, obj, x, table, objvec, model, bfs = lp.simplex(verbose=False)
            else:
                solution, obj, x, table, objvec, model, bfs = lp.simplex_phase2(False, self.simplex_table, self.simplex_model, self.simplex_bfs)
                self.new_constraints = []

            self.simplex_solution, self.simplex_obj, self.simplex_vars, self.simplex_table, self.simplex_objvec, self.simplex_model, self.simplex_bfs = solution, obj, x, table, objvec, model, bfs
            #print("solution", solution)
            #print("obj", obj)
            print("x", x)
            print(len(x))
            for var_idx in range(len(x)):
                vname = 'x_' + str(var_idx)
                v1, v2 = self.vname_edge[vname][0], self.vname_edge[vname][1]
                # copy values from simplex into post-process matrix
                self.varvalues[v1][v2] = float(x[var_idx])
                if (self.varvalues[v1][v2] == 1.0):
                    if ((v2, v1) not in self.varvaluesintegral):
                        self.varvaluesintegral.append((v1, v2))
                        self.edge_colors.append('blue')
                elif (self.varvalues[v1][v2] == 0.0):
                    if ((v2, v1) not in self.varvaluesintegral):
                        self.varvaluesintegral.append((v1, v2))
                        self.edge_colors.append('grey')
                else:
                    if ((v2, v1) not in self.varvaluesfractional):
                        self.varvaluesfractional.append((v1, v2))
                        self.edge_colors.append('green')

            self.varvalueedges = self.varvaluesintegral + self.varvaluesfractional

        elif (solver == 'pulp'):
            # PuLP solver
            self.LP.solve()
            pulp.LpStatus[self.LP.status]
            # self values are already set
            # postprocess the LP solutions
            # segregate 1.0, 0.0, and other edges
            for v1 in range(V):
                for v2 in range(V):
                    if (self.get_variable(v1, v2) is not None):
                        self.varvalues[v1][v2] = self.get_variable(v1, v2).varValue
                        if (self.varvalues[v1][v2] == 1.0):
                            if ((v2, v1) not in self.varvaluesintegral):
                                self.varvaluesintegral.append((v1, v2))
                                self.edge_colors.append('blue')
                        elif (self.varvalues[v1][v2] == 0.0):
                            if ((v2, v1) not in self.varvaluesintegral):
                                self.varvaluesintegral.append((v1, v2))
                                self.edge_colors.append('grey')
                        else:
                            if ((v2, v1) not in self.varvaluesfractional):
                                self.varvaluesfractional.append((v1, v2))
                                self.edge_colors.append('green')
                    else:
                        pass
            self.varvalueedges = self.varvaluesintegral + self.varvaluesfractional
        else:
            print('Solver does not exist.')
            exit(0)

    def check_solution(self):
        return self.separation_oracle()

    def print_lp_output(self):
        print(pulp.LpStatus[self.LP.status])
        print(pulp.value(self.LP.objective))

    def print_tour(self):
        Gd = nx.Graph()
        Gd.add_edges_from(self.varvalueedges)
        #print(self.LP)
        nx.draw(Gd, node_size=500, with_labels=True, node_color='white', edge_color=self.edge_colors)
        #plt.show()

    def cutting_plane(self):
        pass

    def check_variable_integrality(self, v1, v2):
        if (self.varvalues[v1][v2] == 1.0) or (self.varvalues[v1][v2] == 0.0):
            return True
        else:
            return False

    def check_edge_one(self, v1, v2):
        return (self.varvalues[v1][v2] == 1.0)

    def check_integrality(self):
        V = self.G.num_vertices
        tour = []
        for v1 in range(V):
            for v2 in range(v1):
                if self.check_variable_integrality(v1, v2):
                    if self.check_edge_one(v1, v2):
                        tour.append((v1, v2))
                else:
                    return False, []
        return True, sorted(tour)

    def get_constraint_components(self, cons):
        return self.LP.constraints[cons].getLb(), self.LP.constraints[cons].getUb(), self.LP.constraints[cons].items()

    def get_random(self, a, b):
        return a + random.random() * (b - a)

    def gomory_floor(self, v):
        if v.is_integer():
            return 0.0
        else:
            return v - math.floor(v)

    def add_good_gomory_cuts(self):
        '''
        Parse the final simplex dictionary and do the gomory trick
        '''
        print("Adding GOOD gomory cuts")

        dims = self.simplex_table.shape
        num_constraints, num_variables = dims[0], dims[1]

        new_constraints = []
        new_rhs = []
        for cc in range(num_constraints):
            new_constraint = []
            possible_gomory_cut = False
            for vv in range(num_variables):
                if not self.simplex_table[cc, vv].is_integer():
                    possible_gomory_cut = True
            if possible_gomory_cut:
                for vv in range(num_variables):
                    new_constraint.append(self.gomory_floor(self.simplex_table[cc, vv]))
                new_constraints.append(new_constraint)

        self.new_constraints = new_constraints
        self.simplex_table = np.concatenate((self.simplex_table, np.array(new_constraints)), axis=0)
        #print(self.simplex_table.shape[0] - num_constraints, "extra gomory cuts added")
        #print(self.simplex_table.shape[0], "total constraints added")
        # table with extra constraints
        # call simplex_phase2

    def get_random_gomory_cut(self):
        print("Adding a RANDOM valid constraint")
        LC = None
        LC_terms = None
        self.pool = self.subtour_cons + self.cutting#+ self.deg2cons

        if (len(self.pool) > 1):
            idx1 = random.randint(0, len(self.pool) - 1)
            idx2 = random.randint(0, len(self.pool) - 1)
            choice = [self.pool[idx1], self.pool[idx2]]
        elif len(self.pool) == 1:
            choice = [self.pool[0]]
        else:
            self.pool = self.subtour_cons + self.cutting + self.deg2cons
            print("Empty Pool To Add RANDOM constraints")
            idx1 = random.randint(0, len(self.pool) - 1)
            idx2 = random.randint(0, len(self.pool) - 1)
            choice = [self.pool[idx1], self.pool[idx2]]
        for C in choice:
            #print(C)
            LC_terms += self.get_random(0, 2) * C
        LC = LC_terms
        if (type(LC.getUb()) == float or type(LC.getUb()) == int) and (type(LC.getLb()) == float or type(LC.getLb()) == int):
            #print("NONENONENONENONENONENONE")
            return None, None
        floor_LC = None
        floor_LC_terms = None
        for item in LC.items():
            term_variable = item[0]
            term_coeff = item[1]
            term_coeff = math.floor(term_coeff)
            floor_LC_terms += term_variable * term_coeff
        if LC.getLb() == None:
            #print("$$$$$$$$$$$$$$$$")
            return LC, LC

        if (floor_LC_terms is None):
            print(floor_LC_terms)
            return None, None
        floor_LC += floor_LC_terms >= math.floor(LC.getLb())
        #print(floor_LC)
        #print(len(self.pool))
        #print("OKOKOKOKOKOKOKOKOKOK")

        print("RANDOM cut found")
        return LC, floor_LC

    def print_constraints(self):
        for cons in self.LP.constraints:
            lb, ub, items = (self.get_constraint_components(cons))
            print(lb, ub, items)

def solve(G, relaxation, solver, gomory='random'):
    Solver = TspSolver(G)
    Relaxation = relaxation
    Solver.create_initial_tsp_LP(Relaxation)
    #Solver.print_constraints()

    iterations = 100
    iteration = 0
    while True:
        if iteration == iterations:
           break
        print()
        print("###")
        print("ITERATION #", iteration + 1)
        Solver.solve_lp(solver)
        # we should have a feasible point by now
        integral, tour = Solver.check_integrality()
        print("Is Integral = ", integral)
        #Solver.print_lp_output()

        if Solver.check_solution(): # the separation oracle
            print()
            print(".......................")
            print("Feasible solution found")
            break
        # adds the cut constraint that is violated (if any exist)
        print("Separation oracle done")
        iteration += 1

    # we should have a feasible point by now
    integral, tour = Solver.check_integrality()

    if (solver == 'pulp'):
        old_objective_value = pulp.value(Solver.LP.objective)
    else:
        old_objective_value = Solver.simplex_obj
    if integral:
        print("LUCKY! Integral solution found")
        tsp_cost = 0.0
        costs = []
        for edge in tour:
            costs.append(G.adjacency_matrix[edge[0]][edge[1]])
            tsp_cost += G.adjacency_matrix[edge[0]][edge[1]]
        print("EDGES COSTS:", costs, "Tsp cost:", tsp_cost)
        '''
        for variable in Solver.LP.variables():
            print("{} = {}".format(variable.name, variable.varValue))

        '''
        #Solver.print_constraints()
        return tour, old_objective_value
    else:
        print("Fractional solution found")
        print("Use cutting planes to arrive at integral solution")

        while True:
            if (gomory == 'good'):
                # solve via simplex
                # add good gomory cut constraints
                Solver.add_good_gomory_cuts()
            elif (gomory == 'random'):
                LC, floor_LC = Solver.get_random_gomory_cut()
            else:
                print("Invalid cutting plane strategy")
                exit(0)
            if gomory == 'good':
                Solver.solve_lp(solver, gomory_stage=True)
            else:
                if LC is not None and floor_LC is not None:
                    Solver.LP += LC
                    Solver.LP += floor_LC
                    Solver.solve_lp(solver)
                last = list(Solver.LP.constraints.keys())[-1]
                slast = list(Solver.LP.constraints.keys())[-2]
                #print(LC)
                #print(floor_LC)

            if gomory == 'random' and len(Solver.pool) == 1:
                print("Checking if any more min cuts of size less than 2")
                ans = Solver.check_solution()
                print("Separation oracle done")
            if True:
                curr_obj_value = 0
                if (solver == 'pulp'):
                    curr_obj_value = pulp.value(Solver.LP.objective)
                else:
                    curr_obj_value = Solver.simplex_obj

                if  solver == 'pulp' and gomory == 'random' and curr_obj_value <= old_objective_value:
                    #print("UNSUCCESSUL OBJECTIVE VALUES", pulp.value(Solver.LP.objective), old_objective_value)
                    #print(len(Solver.LP.constraints.keys()))
                    Solver.LP.constraints[last].clear()
                    Solver.LP.constraints[last].changeRHS(0)
                    Solver.LP.constraints[slast].clear()
                    Solver.LP.constraints[slast].changeRHS(0)
                    del Solver.LP.constraints[last]
                    del Solver.LP.constraints[slast]
                    continue
                if  solver == 'pulp' and gomory == 'random':
                    Solver.cutting.append(LC)
                    Solver.cutting.append(floor_LC)
                    print("***OBJECTIVE VALUES", pulp.value(Solver.LP.objective), old_objective_value)
                    #print(LC, floor_LC)

                if (solver == 'pulp'):
                    curr_obj_value = pulp.value(Solver.LP.objective)
                    old_objective_value = pulp.value(Solver.LP.objective)
                else:
                    old_objective_value = Solver.simplex_obj
                integral, tour = Solver.check_integrality()
                if integral:
                    print()
                    print(".......................")
                    print("Integral solution found")
                    print(tour)
                    tsp_cost = 0.0
                    costs = []
                    for edge in tour:
                        costs.append(G.adjacency_matrix[edge[0]][edge[1]])
                        tsp_cost += G.adjacency_matrix[edge[0]][edge[1]]
                    print("EDGES COSTS:", costs, "Tsp cost:", tsp_cost)
                    #Solver.print_constraints()
                    return tour, old_objective_value
                else:
                    continue
            else:
                print("Checking if any more min cuts of size less than 2")
                ans = Solver.check_solution()
                print("Separation oracle done")
        '''
        for variable in Solver.LP.variables():
            print("{} = {}".format(variable.name, variable.varValue))
        print(Solver.LP)

        '''
    # @TODO implement cutting planes
    Solver.print_tour()

if __name__ == "__main__":
    V = 20
    G = graph.Graph(V)
    G.init_complete()
    #G.init_adjacency(graph_examples.bad_instance_pulp_random_gomory)
    #G.init_adjacency(graph_examples.subtour_graph)
    #G.init_adjacency(graph_examples.color_graph)
    #G.init_adjacency(graph_examples.infloop)
    #G.init_adjacency(graph_examples.frac)
    print(G)

    if (False):
        ilp_tour, ilp_value = solve(G, relaxation=False, solver='pulp', gomory='random')
        rlp_tour, rlp_value = solve(G, relaxation=True, solver='pulp', gomory='random')

        print("************************************************************************")
        print("Integer LP VALUE =", ilp_value)
        print("Integer LP TOUR =", ilp_tour)
        print("************************************************************************")
        print("Relaxation LP VALUE = ", rlp_value)
        print("Relaxation LP TOUR", rlp_tour)
        print("************************************************************************")

    if (True):
        simplex_tour, simplex_value = solve(G, relaxation=True, solver='simplex', gomory='random')
        print("Simplex VALUE = ", simplex_value)
        print("Simplex TOUR", simplex_tour)
